import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:ouattaraaliapp/rest/repository/UserRepository.dart';
import 'package:ouattaraaliapp/rest/response/UserResult.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  TextEditingController firstNameController =  new TextEditingController();
  TextEditingController lastNameController =  new TextEditingController();
  TextEditingController emailController =  new TextEditingController();
  TextEditingController passwordController =  new TextEditingController();
  TextEditingController confirmPasswordController =  new TextEditingController();

  bool _loading = true;



  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: new Stack(
        children: <Widget>[
          new Center(
            child: new Column(
              children: <Widget>[
                //***********IMAGE POUR INSCRIPTION*******

                new Container(
                  child: Image(

                    image: AssetImage('images/avatar.png'),
                  ),
                  margin: EdgeInsets.only(top: 30.0),
                ),


                new Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Text("FORMULAIRE D'INSCRIPTION",style: TextStyle(fontWeight: FontWeight.bold,fontSize:18.0),),
                ),

                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: new TextField(
                    controller: lastNameController,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person_outline),
                        hintText: 'Nom',
                        border: OutlineInputBorder()
                    ),
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: new TextField(
                    controller: firstNameController,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      hintText: 'Prenom',
                      border: OutlineInputBorder(),

                    ),
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: new TextField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.mail_outline),
                        border: OutlineInputBorder(),
                        hintText: 'Adresse Email'
                    ),
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: new TextField(
                    controller: passwordController,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      border: OutlineInputBorder(),
                      hintText: 'Mot de passe',
                    ),
                  ),
                ),

                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  child: new TextField(
                    controller: confirmPasswordController,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      border: OutlineInputBorder(),
                      hintText: 'Confirmer votre Mot de passe',
                    ),
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  height: 70.0,
                  width: double.infinity,
                  child: new RaisedButton(
                    onPressed: (){


                    print('Nom: ${lastNameController.text.trim()}');
                    print('Prénom: ${firstNameController.text.trim()}');

                    registerUser();



                  },
                    color: Colors.deepPurple,
                    child: Text('INSCRIPTION',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
                  ),
                ),

                new Container(
                  margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                  height: 70.0,
                  width: double.infinity,
                  child: new RaisedButton(onPressed: (){
                  },
                    color: Colors.pink,
                    child: Text('CONNEXION',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),),
                  ),
                ),

              ],
            ),
          ),
          Offstage(
            offstage: _loading,
            child: progressWidget(),
          )
        ],
      ),
    );
  }

  void registerUser() {


    var formBody = {
      "first_name": firstNameController.text.trim(),
      "last_name": lastNameController.text.trim(),
      "password":passwordController.text.trim(),
      "email":emailController.text.trim()
    };

    UserRepository.store(showProgress, hideProgress, formBody)
        .then((dynamic value){


          if(value is UserResult){

            print('Executé avec succes ${value}');

          }

          if(value is Response){
            print('Executé avec erreur ${value.body}');


          }

          //TODO Enregister dans la base de donnée du téléphonne

         // Aller à l'écran HomeScreen



         })
        .catchError((error){

          print('Erreur rencontrée $error');


         });




  }

  void showProgress() {



    print('Loading ${_loading}');

    if(mounted){
      setState(() {
        _loading = false;
      });
    }


  }

  void hideProgress() {



    if(mounted){
      setState(() {
        _loading = true;
      });
    }

  }

  Widget progressWidget(){

    return new Stack(
      children: <Widget>[

        new Container(
          color: Colors.white,
        ),

        new Center(
          child: new CircularProgressIndicator(),
        )

      ],
    );
  }
}
