import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:ouattaraaliapp/models/Conversation.dart';
import 'package:ouattaraaliapp/models/Customer.dart';
import 'package:ouattaraaliapp/models/Message.dart';
import 'package:ouattaraaliapp/models/User.dart';
import 'package:ouattaraaliapp/rest/repository/ConversationRepository.dart';
import 'package:ouattaraaliapp/rest/response/ConversationResult.dart';
import 'package:ouattaraaliapp/rest/response/MessageListResult.dart';

class ConversationScreen extends StatefulWidget {

  User user;

  Conversation conversation;

  Customer receiver;

  ConversationScreen(this.user, this.conversation, this.receiver);

  @override
  _ConversationScreenState createState() => _ConversationScreenState();

}

class _ConversationScreenState extends State<ConversationScreen> {

  Conversation _conversation;

  List<Message>  _messages = [];

  @override
  void initState() {
    _conversation = widget.conversation;
    super.initState();

    Timer.run(() {

      findOrCreateConversation(widget.user, widget.receiver);

    });


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_conversation !=null && _conversation.getTitle(widget.user) != null ? _conversation.getTitle(widget.user) :getCustomerName(widget.receiver)),
      ),
    );
  }

 String  getCustomerName(Customer user) {

    if(user != null && user.first_name != null && user.last_name != null){

      return "${user.first_name} ${user.last_name}";
    }else{

      return "Utilisateur Inconnu";

    }
  }


  void findOrCreateConversation(User connectedUser, Customer receiver) {

    if(connectedUser == null){
      print('connectedUser is null');
      return;
    }

    if(receiver == null){
      print('receiver is null');
      return;
    }


    print('FirstOrCreate started');

    ConversationRepository.firstOrCreate([connectedUser.id, receiver.id])
        .then((value){

      if(value != null && value is ConversationResult){

        print('FirstOrCreate Response >>> ${value.message}');

        if(value.success && value.data != null){

          if(mounted){
            setState(() {
              _conversation  = value.data;
            });
          }
        }

      }

      if(value != null && value is Response){

        print('FirstOrCreate Response >>> ${value.body}');

      }

    })
        .catchError((error){


      print('Error found $error');

    });

  }

  void loadMessageByConversationId(int id){
    ConversationRepository.findMessagesById(id, (){}, (){})
        .then((value){

      if(value != null && value is MessageListResult){

        print('loadMessageByConversationId Response >>> ${value.message}');

        if(value.success && value.data != null){

          if(mounted){
            setState(() {
              _messages  = value.data;
            });
          }
        }

      }

      if(value != null && value is Response){

        print('loadMessageByConversationId Response >>> ${value.body}');

      }

    })
        .catchError((error){


      print('Error found $error');

    });


  }


}
