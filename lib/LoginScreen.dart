import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:ouattaraaliapp/HomeScreen.dart';
import 'package:ouattaraaliapp/RegisterScreen.dart';
import 'package:ouattaraaliapp/models/User.dart';
import 'package:ouattaraaliapp/rest/repository/UserRepository.dart';
import 'package:ouattaraaliapp/rest/response/AuthResult.dart';

import 'package:toast/toast.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  bool _loading = true;

  TextEditingController emailController = new TextEditingController(text: 'angebagui@gmail.com');
  TextEditingController passwordController = new TextEditingController(text: '123456789');


  @override
  Widget build(BuildContext context) {

    return new Scaffold(

      body: new Stack(
        children: <Widget>[

          new Center(
            child: new Column(
              children: <Widget>[
                new Container(
                  width:  90.0,
                  height: 90.0,
                  margin: EdgeInsets.only(top: 50.0),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    shape: BoxShape.circle,
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 20.0,left: 20.0,right: 20.0),
                  alignment: Alignment.center,
                  child: new Text('Connectez Vous !', style: TextStyle(color: Colors.black,fontSize: 20.0,fontWeight: FontWeight.bold)),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 20.0,left: 20.0,right: 20.0),
                  alignment: Alignment.center,
                  child: new Text('Rejoignez la communauté des meilleurs !', style: TextStyle(color: Colors.black,fontSize: 20.0,fontWeight: FontWeight.bold)),
                ),
                new Container(
                  margin: EdgeInsets.only(top: 20.0,left: 20.0,right: 20.0),
                  child: new TextField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                        prefixIcon: Icon( Icons.email),
                        hintText: 'Email',
                        border: OutlineInputBorder()
                    ),

                  ),

                ),
                new Container(
                  margin: EdgeInsets.only(top: 15.0,left: 15.0,right: 15.0),

                  child: TextField(
                    controller: passwordController,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      hintText: 'Mot de passe',
                      border: OutlineInputBorder(),

                    ),

                  ),

                ),
                new Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Text('Mot de passe Oublié ?', style: TextStyle(fontSize: 16.0,decoration: TextDecoration.underline),),
                ),
                new Container(
                  height: 60.0,
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 20.0,left: 20.0,right: 20.0),
                  child: new RaisedButton(
                    onPressed: (){

                      connectUser();


                    },
                    color: Colors.pink,
                    child: Text('CONNEXION',style: TextStyle(color: Colors.white),),
                  ),
                ),

                new Container(
                  height: 60.0,
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 15.0,left: 15.0,right: 15.0),
                  child: new RaisedButton(onPressed: (){

                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return new RegisterScreen();
                    }));
                  },
                    color: Colors.blue,
                    child: Text('INSCRIPTION ',style: TextStyle(color: Colors.white),),
                  ),
                ),
              ],
            ),
          ),

          new Offstage(
            offstage: _loading,
            child: progressWidget(),
          )

        ],
      )
    );
  }

  void connectUser() {

    Map<String, String> formBody = {
      "email": emailController.text.trim(),
      "password": passwordController.text.trim()
    };
    UserRepository.login(showProgress, hideProgress, formBody)
    .then((value){

      if(value is Response){

        Toast.show("Mot de passe incorrect", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

      }

      if(value is AuthResult){

        if(value.success){

          Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context)=>HomeScreen(User.fromJson(value.data.toJson()))));

        }

      }

    })
    .catchError((error){

      hideProgress();


      print('Error $error');

      if(error is SocketException){
        Toast.show("Vous n'avez pas accès à Internet", context, duration: Toast.LENGTH_LONG);

      }else{

        Toast.show("Nous rencontrons des problèmes avec le serveur", context, duration: Toast.LENGTH_LONG);
      }

    });


  }

  void showProgress() {



    print('Loading ${_loading}');

    if(mounted){
      setState(() {
        _loading = false;
      });
    }


  }

  void hideProgress() {


    if(mounted){
      setState(() {
        _loading = true;
      });
    }

  }

  Widget progressWidget(){

    return new Stack(
      children: <Widget>[

        new Container(
          color: Colors.white,
        ),

        new Center(
          child: new CircularProgressIndicator(),
        )

      ],
    );
  }
}
