// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Conversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Conversation _$ConversationFromJson(Map<String, dynamic> json) {
  return Conversation(
    json['id'] as int,
    (json['speakers'] as List)?.map((e) => e as int)?.toList(),
    json['is_group'] as bool,
    json['group_name'] as String,
    json['admins'] as String,
    (json['users'] as List)
        ?.map((e) =>
            e == null ? null : Customer.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['messages'] as List)
        ?.map((e) =>
            e == null ? null : Message.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['created_at'] as String,
    json['updated_at'] as String,
    json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$ConversationToJson(Conversation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'speakers': instance.speakers,
      'is_group': instance.is_group,
      'group_name': instance.group_name,
      'admins': instance.admins,
      'users': instance.users,
      'messages': instance.messages,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
    };
