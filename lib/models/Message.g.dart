// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    json['id'] as int,
    json['content'] as String,
    json['sender_id'] as int,
    json['conversation_id'] as int,
    json['is_read'] as bool,
    json['is_received'] as bool,
    json['is_sent'] as bool,
    json['content_type'] as String,
    json['sender'] == null
        ? null
        : Customer.fromJson(json['sender'] as Map<String, dynamic>),
    json['created_at'] as String,
    json['updated_at'] as String,
    json['deleted_at'] as String,
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'sender_id': instance.sender_id,
      'conversation_id': instance.conversation_id,
      'is_read': instance.is_read,
      'is_received': instance.is_received,
      'is_sent': instance.is_sent,
      'content_type': instance.content_type,
      'sender': instance.sender,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
      'deleted_at': instance.deleted_at,
    };
