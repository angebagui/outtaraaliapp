import 'package:json_annotation/json_annotation.dart';
import 'package:ouattaraaliapp/models/Customer.dart';
import 'package:ouattaraaliapp/models/Message.dart';
import 'package:ouattaraaliapp/models/User.dart';

part 'Conversation.g.dart';

@JsonSerializable()
class Conversation{

  int id;

 List<int> speakers;

 bool is_group;

 String group_name;

 String admins;

  List<Customer> users;

  List<Message> messages;

  String created_at;

  String updated_at;

  String deleted_at;


  Conversation(this.id, this.speakers, this.is_group, this.group_name,
      this.admins, this.users, this.messages, this.created_at, this.updated_at,
      this.deleted_at);

  factory Conversation.fromJson(Map<String, dynamic> json) => _$ConversationFromJson(json);
  Map<String, dynamic> toJson() => _$ConversationToJson(this);

  String getTitle(User user) {
   if(user != null && speakers !=null ){

    if(speakers.contains(user.id) && users != null && users.isNotEmpty){

     if(is_group){

      return group_name;
     }else{
      Customer receiver = users.singleWhere((element) => element.id != user.id);

      if(receiver != null){

       return "${receiver.first_name} ${receiver.last_name}";

      }else{

       return '';

      }

     }

    }else{
     return '';
    }

   }else{

    return '';

   }
  }



}