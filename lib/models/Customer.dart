import 'package:json_annotation/json_annotation.dart';

part 'Customer.g.dart';

@JsonSerializable()
class Customer{

  int id;

  String first_name;

  String last_name;

  String email;

  String password;

  String photo;

  String created_at;

  String updated_at;

  String deleted_at;

  Customer(this.id, this.first_name, this.last_name, this.email, this.password,
      this.photo, this.created_at, this.updated_at, this.deleted_at);

  factory Customer.fromJson(Map<String, dynamic> json) => _$CustomerFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerToJson(this);




}