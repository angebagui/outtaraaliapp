import 'package:ouattaraaliapp/models/Customer.dart';

import 'package:json_annotation/json_annotation.dart';

part 'Message.g.dart';

@JsonSerializable()
class Message{

  int id;
  String content;
  int sender_id;
  int conversation_id;
  bool is_read;
  bool is_received;
  bool is_sent;
  String content_type;
  Customer sender;

  String created_at;

  String updated_at;

  String deleted_at;

  Message(this.id, this.content, this.sender_id, this.conversation_id,
      this.is_read, this.is_received, this.is_sent, this.content_type,
      this.sender, this.created_at, this.updated_at, this.deleted_at);
  

  factory Message.fromJson(Map<String, dynamic> json) => _$MessageFromJson(json);
  Map<String, dynamic> toJson() => _$MessageToJson(this);


}