class User{

  int id;
  String first_name;
  String last_name;
  String email;
  String password;
  String photo;
  String created_at;

  User(this.id, this.first_name, this.last_name,
      this.email, this.password,
      this.photo, this.created_at);

  static User fromJson(Map map){

    return new User(
        map["id"] as int,
        map["first_name"] as String,
        map["last_name"] as String,
        map["email"] as String,
        map["password"] as String,
        map["photo"] as String,
        map["created_at"] as String
    );
  }

  @override
  String toString() {
    return 'User{id: $id, first_name: $first_name, last_name: $last_name, email: $email, password: $password, photo: $photo, created_at: $created_at}';
  }


}