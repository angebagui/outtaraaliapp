import 'package:flutter/material.dart';
import 'package:ouattaraaliapp/ListContactScreen.dart';
import 'package:ouattaraaliapp/models/User.dart';



class HomeScreen extends StatefulWidget {

  User user;


  HomeScreen(this.user);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: new Scaffold(
        appBar:AppBar(
          title: Text("Search"),
          centerTitle: false,
          bottom: TabBar(
            tabs: <Widget>[
              new Tab(text:'Message'),
              new Tab(text: 'Group',),
            ],
          ),
          actions: <Widget>[
            IconButton(
              onPressed: (){
              },
              icon: Image.asset('images/menu.png',color: Colors.white,),
            ),
          ],
        ),

       floatingActionButton: new FloatingActionButton(
           child: Icon(Icons.add),
           onPressed: (){

             Navigator.push(context, new MaterialPageRoute(builder: (context)=> ListContactScreen(widget.user)));

       }) ,
       bottomNavigationBar: new BottomNavigationBar(
           onTap: (int position){
          },
           items: [
             new BottomNavigationBarItem(icon: Image.asset('images/add_button.png',),title:Text('')),
             new BottomNavigationBarItem(icon: Image.asset('images/friend_icon.png',),title:Text('')),
             new BottomNavigationBarItem(icon: Image.asset('images/messag_and_group.png',),title:Text('')),
             new BottomNavigationBarItem(icon: Image.asset('images/notifications_icon.png',),title:Text('')),
             new BottomNavigationBarItem(icon: Image.asset('images/profile_icon.png',),title:Text('')),
 ]),
        body: new TabBarView(
            children:[
              new Column(
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(top: 20.0),
                    child: ListTile(
                      leading: new Stack(
                        children: <Widget>[
                          new Container(
                            width: 60.0,
                            height: 60.0,
                            decoration: BoxDecoration(
                                color: Colors.black,
                                shape: BoxShape.circle
                            ),
                          ),
                          Positioned.fill(
                            child: new Container(
                              width: 20.0,
                              height: 20.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.green,
                                border: Border.all(color:Colors.white,width: 5.0),
                              ),
                            ),
                            top: 35.0,
                            right: 28.0,
                          ),
                        ],
                      ),

                      title: Text('OUATTARA ALI',style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Text('Hey when are you going ?'),
                      trailing: new Container(
                        width: 60.0,
                        height: 60.0,
                        child: Column(
                          children: <Widget>[
                            Text('9:45AM'),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              padding: EdgeInsets.only(
                                top:5.0,
                                bottom: 5.0,
                                left: 16.0,
                                right: 16.0,
                              ),

                              child: Text('14',style: TextStyle(color: Colors.white),),

                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(30.0)
                              ),
                            )

                          ],

                        ),

                      ),

                    ),

                  ),
                  new Container(

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        new Stack(
                          children: <Widget>[
                            new Container(
                              margin: EdgeInsets.only(left: 18.0),
                              width: 58.0,
                              height: 58.0,
                              decoration: BoxDecoration(
                                color: Colors.black,
                                shape: BoxShape.circle,
                              ),
                            ),
                            Positioned.fill(
                                child: new Container(
                                  margin: EdgeInsets.only(top: 39.0,right: 20.0),
                                  width: 20.0,
                                  height: 20.0,
                                  decoration: BoxDecoration(
                                    color: Colors.green,
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.white,width: 5.0),
                                  ),
                                )
                            )
                          ],
                        ),
                        new Expanded(
                          child: Container(
                            padding: EdgeInsets.only(left: 16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  child: Text('Etham Walker',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                                ),
                                new Container(
                                  child: Text('What are you douing ?',style: TextStyle(fontSize: 18),),
                                ),
                              ],
                            ),
                          ),),





                        new Container(
                          child: new Column(
                            children: <Widget>[
                              new Container(
                                margin: EdgeInsets.only(right: 15.0),
                                child: Text("9H15AM"),
                              ),

                              new Container(
                                margin: EdgeInsets.only(right: 15.0,top: 5.0),
                                padding: EdgeInsets.only(top: 6.0,bottom: 3.0,left: 16.0,right: 16.0),
                                width: 52.0,
                                height: 28.0,
                                child: Text('14',style: TextStyle(color: Colors.white),),
                                decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(30.0),


                                ),
                              ),
                            ],
                          ),
                        )


                      ],

                    ),
                  ),




                ],
              ),
              new Container(
                color: Colors.amber,
              )
            ] ),

      ),
    );
  }
}
