import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreenExo extends StatefulWidget {
  @override
  _HomeScreenExoState createState() => _HomeScreenExoState();
}

class _HomeScreenExoState extends State<HomeScreenExo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Exercice"),
        backgroundColor:Colors.deepPurple,
      ),
      body: new Column(
        children: <Widget>[
          new Container(
            child: ListTile(
              leading: new Container(
                margin: EdgeInsets.only(top: 10.0),
                width:60.0,
                height: 60.0,
                decoration: BoxDecoration(
                  shape:BoxShape.circle,
                  color: Colors.black,
                ),
              ),
              title: Text("OUATTARA ALI",style:TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text('Bonjour à tous'),
              trailing: new Container(
                margin: EdgeInsets.only(top:7.0),
                child: Column(
                  children: <Widget>[
                   Text('15H45',style: TextStyle(fontWeight: FontWeight.bold),),
                    new Container(
                      child: Text("20",style: TextStyle(color: Colors.white),),
                      width: 40.0,
                      height: 20.0,
                      color: Colors.black,
                      
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
