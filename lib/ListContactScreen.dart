import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:ouattaraaliapp/ConversationScreen.dart';
import 'package:ouattaraaliapp/models/Customer.dart';
import 'package:ouattaraaliapp/models/User.dart';
import 'package:ouattaraaliapp/rest/repository/UserRepository.dart';
import 'package:ouattaraaliapp/rest/response/ListContactResult.dart';

class ListContactScreen extends StatefulWidget {

  User user;

  ListContactScreen(this.user);

  @override
  _ListContactScreenState createState() => _ListContactScreenState();

}

class _ListContactScreenState extends State<ListContactScreen> {

  bool _loading = true;

  List<Customer> contacts = [];

  TextEditingController _queryController = new TextEditingController();

  @override
  void initState() {
    super.initState();

    _queryController.addListener(() {

      String query = _queryController.text;

      print('Query $query');

      if(query != null && query.length >3 ){
        searchContact(query);

      }


    });

    Timer.run(() {

      loadContact();

    });
  }


  @override
  Widget build(BuildContext context) {

    if(contacts == null){
      contacts = [];
    }

    return new Scaffold(
      appBar: AppBar(
        title: new Text('Rechercher Contact'),
        elevation: 0.0,
      ),

      body: new Stack(
        children: <Widget>[
          new Column(
            children: <Widget>[

              new Container(
                height: 70.0,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor
                ),
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child:  new Column(
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(60.0)
                      ),
                      padding: EdgeInsets.only(left:20.0, right: 20.0),
                      child: new TextField(
                        controller: _queryController,
                        decoration: InputDecoration(
                            hintText: 'Rechercher',
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                        ),keyboardType: TextInputType.text,
                      ),
                    )
                  ],
                ),
              ),
              new Expanded(child: ListView(
                  children: contacts.map((contact) =>
                      ListTile(
                        onTap: (){

                          Navigator.push(context, new MaterialPageRoute(builder: (context)=> ConversationScreen(widget.user,null,contact)));

                        },
                    leading: new Container(
                      width: 60.0,
                      height: 60.0,
                      margin: EdgeInsets.only(top: 10.0),
                      decoration: BoxDecoration(
                          color: Colors.black,
                          shape: BoxShape.circle
                      ),
                    ),
                    title: new Text(contact != null && contact.first_name != null && contact.last_name !=null ?'${contact.first_name} ${contact.last_name}':""),
                    subtitle: new Text(contact != null && contact.email != null ?'${contact.email}':""),

                  )).toList()
              )),

            ],
          ),
          new Offstage(
            offstage: _loading,
            child: progressWidget(),
          )
        ],
      )
    );
  }

  void loadContact() {

      UserRepository.listContacts(showProgress, hideProgress)
      .then((value){

        if(value is ListContactResult){

          if(value.success && value.data !=null){

            setState(() {
              contacts = value.data;
            });
          }
        }

        if(value is Response){



        }

      })
      .catchError((error){

        print("Error found $error");

      });


  }

  void showProgress() {

    print('Loading ${_loading}');

    if(mounted){
      setState(() {
        _loading = false;
      });
    }


  }

  void hideProgress() {


    if(mounted){
      setState(() {
        _loading = true;
      });
    }

  }

  Widget progressWidget(){

    return new Stack(
      children: <Widget>[

        new Container(
          color: Colors.white,
        ),

        new Center(
          child: new CircularProgressIndicator(),
        )

      ],
    );
  }

  void searchContact(String query) {


    UserRepository.searchContact(showProgress, hideProgress, query)
        .then((value){

      if(value is ListContactResult){

        if(value.success && value.data !=null){

          setState(() {
            contacts = value.data;
          });
        }
      }

      if(value is Response){



      }

    })
        .catchError((error){

      print("Error found $error");

    });



  }
}
