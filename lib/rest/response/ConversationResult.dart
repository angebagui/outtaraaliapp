

import 'package:ouattaraaliapp/models/Conversation.dart';

class ConversationResult{

  bool success;

  String message;

  Conversation data;

  ConversationResult(this.success, this.message, this.data);

  static ConversationResult fromJson(Map e) {

    return new ConversationResult(e['success'] as bool, e['message'] as String, Conversation.fromJson(e['data'] as Map));
  }


}
