
import 'package:ouattaraaliapp/models/Message.dart';

class MessageListResult{

  bool success;

  String message;

  List<Message> data;


  MessageListResult(this.success, this.message, this.data);

  static MessageListResult fromJson(Map e) {

    return new MessageListResult(
        e['success'] as bool,
        e['message'] as String,
        (e['data'] as List).map((dynamic element){
          return Message.fromJson(element as Map);
        }).toList()
    );
  }

}
