import 'package:ouattaraaliapp/models/User.dart';

class UserResult{

  bool success;

  String message;

  User data;

  UserResult(this.success, this.message, this.data);

  static UserResult fromJson(Map map){

    return new UserResult(map["success"] as bool, map["message"] as String, User.fromJson(map['data'] as Map));
  }

  static List<User> transformListToUsers(dynamic value) {

    List<User> users = [];

    if(value != null){

      List list = value as List;


      users = list.map((dynamic element){

        return User.fromJson(element as Map);

      });

    }


    return users;

  }

  @override
  String toString() {
    return 'UserResult{success: $success, message: $message, data: $data}';
  }


}