import 'package:ouattaraaliapp/models/Customer.dart';
import 'package:json_annotation/json_annotation.dart';



part 'ListContactResult.g.dart';

@JsonSerializable()
class ListContactResult{

  bool success;

  String message;

  List<Customer> data;

  ListContactResult(this.success, this.message, this.data);


  factory ListContactResult.fromJson(Map<String, dynamic> json) => _$ListContactResultFromJson(json);

  Map<String, dynamic> toJson() => _$ListContactResultToJson(this);
  
}