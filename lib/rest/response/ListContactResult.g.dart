// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ListContactResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListContactResult _$ListContactResultFromJson(Map<String, dynamic> json) {
  return ListContactResult(
    json['success'] as bool,
    json['message'] as String,
    (json['data'] as List)
        ?.map((e) =>
            e == null ? null : Customer.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ListContactResultToJson(ListContactResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
