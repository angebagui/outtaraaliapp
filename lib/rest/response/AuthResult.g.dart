// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AuthResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthResult _$AuthResultFromJson(Map<String, dynamic> json) {
  return AuthResult(
    json['success'] as bool,
    json['message'] as String,
    json['data'] == null
        ? null
        : Customer.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AuthResultToJson(AuthResult instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'data': instance.data,
    };
