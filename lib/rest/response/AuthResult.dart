import 'package:ouattaraaliapp/models/Customer.dart';

import 'package:json_annotation/json_annotation.dart';



part 'AuthResult.g.dart';

@JsonSerializable()
class AuthResult{

  bool success;

  String message;


  Customer data;

  AuthResult(this.success, this.message, this.data);

  factory AuthResult.fromJson(Map<String, dynamic> json) => _$AuthResultFromJson(json);
  Map<String, dynamic> toJson() => _$AuthResultToJson(this);


}