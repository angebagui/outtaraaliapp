import 'package:ouattaraaliapp/models/Conversation.dart';

class ConversationListResult{

  bool success;

  String message;

  List<Conversation> data;

  ConversationListResult(this.success, this.message, this.data);

  static ConversationListResult fromJson(Map e) {

    return new ConversationListResult(
        e['success'] as bool,
        e['message'] as String,
        e['data'] == null? [] : (e['data'] as List).map((dynamic element){
          return Conversation.fromJson(element as Map);
        }).toList()

    );
  }


}
