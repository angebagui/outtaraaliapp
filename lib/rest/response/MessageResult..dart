

import 'package:ouattaraaliapp/models/Message.dart';

class MessageResult{

  bool success;

  String message;

  Message data;


  MessageResult(this.success, this.message, this.data);

  static MessageResult fromJson(Map e) {

    return new MessageResult(
        e['success'] as bool,
        e['message'] as String,
        Message.fromJson((e['data'] as Map))

    );
  }

}
