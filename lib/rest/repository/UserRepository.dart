import 'dart:convert';

import 'package:http/http.dart';
import 'package:ouattaraaliapp/rest/response/AuthResult.dart';
import 'package:ouattaraaliapp/rest/response/ListContactResult.dart';
import 'package:ouattaraaliapp/rest/response/UserResult.dart';

class UserRepository{


  static Future listContacts(Function show, Function hide)async{

    show();

    String url = 'http://socialapp.adj.ci/api/v1/customers';

    Response response = await get(url);

    hide();

    if(response.statusCode == 200){

      Map resultMap = jsonDecode(response.body);

      return  ListContactResult.fromJson(resultMap);

    }else{

      return response;
    }

  }



  static Future login(Function show,  Function hide, Map<String, String> formBody) async{

    show();

    String url = 'http://socialapp.adj.ci/api/v1/user_auth';

    Response response = await post(url, body: formBody);

    hide();

    if(response.statusCode >= 200 && response.statusCode <300){

      Map json = jsonDecode(response.body);

      return AuthResult.fromJson(json);

    }else{

      return response;

    }
  }

  static Future store(
      Function showProgressBar,
      Function hideProgressBar,
      Map<String, String> formBody
      )async{

    showProgressBar();


    //Executer la requete vers le version

    String url = "http://socialapp.adj.ci/api/v1/user_register";
    Response res = await post(url, body: formBody);

    hideProgressBar();

    if( res.statusCode >= 200 && res.statusCode < 300){

      Map resultMap = jsonDecode(res.body);

      return  UserResult.fromJson(resultMap);

    }else{
      return res;
    }




  }

  static Future searchContact(void Function() show, void Function() hide, String query) async{
    show();

    String url = 'http://socialapp.adj.ci/api/v1/customers?first_name=$query';

    Response response = await get(url);

    hide();

    if(response.statusCode == 200){

      Map resultMap = jsonDecode(response.body);

      return  ListContactResult.fromJson(resultMap);

    }else{

      return response;
    }
  }




}