import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ouattaraaliapp/rest/response/ConversationListResult.dart';
import 'package:ouattaraaliapp/rest/response/ConversationResult.dart';
import 'package:ouattaraaliapp/rest/response/MessageListResult.dart';
import 'package:ouattaraaliapp/rest/response/MessageResult..dart';

class ConversationRepository{


  static Future firstOrCreate(List<int> speakers) async{

    var url = 'http://socialapp.adj.ci/api/v1/open_conversation';

    var response = await http.post(url,

        body: jsonEncode({
          "speakers": speakers // "[3,4]"
        }),
        headers: {
          'Content-type' : 'application/json',
          'Accept': 'application/json',
        }
    );

    print('Body >>> ${response.body}');

    if(response.statusCode == 200){

      Map jsonResult = jsonDecode(response.body);

      return ConversationResult.fromJson(jsonResult);


    }else{
      return response;
    }

  }
  static Future findMessagesById(int conversationId, Function showProgress, Function hideProgress)async{

    showProgress();

    var url = 'http://socialapp.adj.ci/api/v1/conversations/messages/$conversationId';

    var response = await http.get(url);

    hideProgress();

    if(response.statusCode == 200){

      Map jsonResult = jsonDecode(response.body);

      return MessageListResult.fromJson(jsonResult);

    }else{

      return response;
    }

  }

  static Future sendMessage(Map<String, dynamic> formFields)async{

    var url = 'http://socialapp.adj.ci/api/v1/messages';

    var response = await http.post(url,
        body: jsonEncode(formFields),
        headers: {
          'Content-type' : 'application/json',
          'Accept': 'application/json',
        }
    );

    if(response.statusCode == 200){

      Map jsonResult = jsonDecode(response.body);

      return MessageResult.fromJson(jsonResult);


    }else{
      return response;
    }

  }

  static Future findAllById(int userId)async {

    var url = 'http://socialapp.adj.ci/api/v1/conversations/customers/$userId';

    var response = await http.get(url);

    if(response.statusCode == 200){

      Map jsonResult = jsonDecode(response.body);

      return ConversationListResult.fromJson(jsonResult);

    }else{

      return response;
    }
  }


}